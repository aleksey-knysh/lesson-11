//
//  ViewController.swift
//  ProtocolsAndDelegates
//
//  Created by Aleksey Knysh on 2/2/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var refrigerator = [Food & Storable]()
    let doctor = Doctor(antibiotic: "антибиотик", potion: "микстура", head: "голова", leg: "нога", belly: "живот")
    let doctorFriend = DoctorsFriend(gooseGrass: "подорожник", valerian: "валерьянка", head: "голова", leg: "нога", belly: "живот")
    
    func sortName (array: [Food]) {
        print("\nСортеруем продукты по алфавиту\n-----------------------------")
        let sort = array.sorted(by: { $0.name < $1.name })
        sort.forEach { print($0.name) }
    }
    
    func sortingProductsInTheRefrigerator (array: [Food & Storable]) {
        let sort = array.sorted { $1.daysToExpire > $0.daysToExpire }
        print("\nСрок годнoсти продуктов\n-----------------------")
        sort.forEach { print("Продукт \($0.name) годен до: \($0.daysToExpire)") }
        print()
    }
    
    func changeOfDoctor(patient: Patients, doctor: Doctor) {
        guard patient.assessmentOfDoctorServices else {
            patient.delegate? = doctor
            return print("Пациент \(patient.name) сменил доктора")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let fruits: Food = Fruits(name: "яблоко")
        let fruits1: Food = Fruits(name: "лимон")
        let fruits2: Food = Fruits(name: "груша")
        let vegetables: Food = Vegetables(name: "капуста")
        let vegetables1: Food = Vegetables(name: "помидор")
        let vegetables2: Food = Vegetables(name: "картофель")
        let sewingProducts: Food = SewingProducts(name: "туфли")
        let sewingProducts1: Food = SewingProducts(name: "футболка")
        let sewingProducts2: Food = SewingProducts(name: "носки")
        let meatProducts: Food & Storable = MeatProducts(name: "свинина", expired: Bool.random(), daysToExpire: 7.02)
        let meatProducts1: Food & Storable = MeatProducts(name: "говядина", expired: Bool.random(), daysToExpire: 2.03)
        let meatProducts2: Food & Storable = MeatProducts(name: "куриное феле", expired: Bool.random(), daysToExpire: 12.04)
        let milkProducts: Food & Storable = MilkProducts(name: "молоко", expired: Bool.random(), daysToExpire: 4.05)
        let milkProducts1: Food & Storable = MilkProducts(name: "сметана", expired: Bool.random(), daysToExpire: 10.06)
        let milkProduct2: Food & Storable = MilkProducts(name: "творог", expired: Bool.random(), daysToExpire: 6.07)
        
        let arrayBag = [fruits, fruits1, fruits2, vegetables, vegetables1, vegetables2, sewingProducts, sewingProducts1, sewingProducts2, meatProducts, meatProducts1, meatProducts2, milkProducts, milkProducts1, milkProduct2]
        
        arrayBag.forEach { $0.test() }
        
        sortName(array: arrayBag)
        
        // кладём продукты в холодильник, а испорченные выкидываем
        arrayBag.forEach {
            if let expiredProduct = $0 as? Food & Storable {
                if expiredProduct.expired == true {
                    //                    refrigerator.append(expiredProduct)
                    print("\n\(expiredProduct.name) испорчен(а,о) - выкидывам в мусорку")
                } else {
                    refrigerator.append(expiredProduct)
                    print("\n\(expiredProduct.name) не испорчен(а,о) - кладём в холодильник")
                }
            }
        }
        
        // сортеруем дату годности продукта
        sortingProductsInTheRefrigerator(array: refrigerator)
        
        // MARK: - Delegates
        let patient1 = Patients(name: "Ваня", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: true, delegate: doctor)
        let patient2 = Patients(name: "Света", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: false, delegate: doctorFriend)
        let patient3 = Patients(name: "Генадий", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: true, delegate: doctor)
        let patient4 = Patients(name: "Алёна", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: false, delegate: doctorFriend)
        let patient5 = Patients(name: "Стас", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: true, delegate: doctor)
        let patient6 = Patients(name: "Анджела", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: false, delegate: doctorFriend)
        let patient7 = Patients(name: "Фёдор", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: true, delegate: doctor)
        let patient8 = Patients(name: "Татьяна", temperature: Bool.random(), cough: Bool.random(), assessmentOfDoctorServices: false, delegate: doctorFriend)
        
        let arrayPatients = [patient1, patient2, patient3, patient4, patient5, patient6, patient7, patient8]
        
        // выводим всех кто заболел
        arrayPatients.forEach { $0.gotSick() }
        
        // лечим всех больных
        arrayPatients.forEach { $0.goToTheDoctor() }
        
        doctorFriend.patientComplaintsReport()
        doctor.patientComplaintsReport()
        
        arrayPatients.forEach { changeOfDoctor(patient: $0, doctor: doctor) }

    }
}
