//
//  Protocols.swift
//  ProtocolsAndDelegates
//
//  Created by Aleksey Knysh on 2/18/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

// MARK: - protocol Food
protocol Food {
    var name: String { get }
    
    func test()
}

// MARK: - protocol Storable
protocol Storable {
    var expired: Bool { get }
    var daysToExpire: Double { get }
}

// MARK: - Fruits
class Fruits: Food {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func test() {
        if name.contains("лимон") {
            print("Это \(name). Пробуем на вкус - кислый")
        } else if name.contains("яблоко") {
            print("Это \(name). Пробуем на вкус - сладкое")
        } else {
            print("Это \(name). Пробуем на вкус - гнилая")
        }
    }
}

// MARK: - Vegetables
class Vegetables: Food {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func test() {
        if name.contains("капуста") {
            print("Это \(name). Пробуем на вкус - вкусная")
        } else if name.contains("помидор") {
            print("Это \(name). Пробуем на вкус - сочный")
        } else {
            print("Это \(name). Пробуем на вкус - пересоленный")
        }
    }
}
// MARK: - sewingProducts
class SewingProducts: Food {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func test() {
        if name.contains("туфли") {
            print("Это \(name). Пробуем на вкус - несъедобные")
        } else if name.contains("футболка") {
            print("Это \(name). Пробуем на вкус - безвкусная")
        } else {
            print("Это \(name). Пробуем на вкус - попросите кого-нибудь другого")
        }
    }
}

// MARK: - MeatProducts
class MeatProducts: Food & Storable {
    var name: String
    var expired: Bool
    var daysToExpire: Double
    
    init(name: String, expired: Bool, daysToExpire: Double) {
        self.name = name
        self.expired = expired
        self.daysToExpire = daysToExpire
    }
    
    func test() {
        if name.contains("свинина") {
            print("Это \(name). Пробуем на вкус - сырая")
        } else if name.contains("говядина") {
            print("Это \(name). Пробуем на вкус - сырая")
        } else {
            print("Это \(name). Пробуем на вкус - сырое")
        }
    }
}

// MARK: - MilkProducts
class MilkProducts: Food & Storable {
    var name: String
    var expired: Bool
    var daysToExpire: Double
    
    init(name: String, expired: Bool, daysToExpire: Double) {
        self.name = name
        self.expired = expired
        self.daysToExpire = daysToExpire
    }
    
    func test() {
        if name.contains("молоко") {
            print("Это \(name). Пробуем на вкус - прокисшее")
        } else if name.contains("сметана") {
            print("Это \(name). Пробуем на вкус - испорченная")
        } else {
            print("Это \(name). Пробуем на вкус - кислый")
        }
    }
}
