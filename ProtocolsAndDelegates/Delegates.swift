//
//  Delegates.swift
//  ProtocolsAndDelegates
//
//  Created by Aleksey Knysh on 2/18/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

// MARK: - delegate
protocol DoctorDelegate: class {
    func treatingPatients(patient: Patients)
}

class Patients {
    let name: String
    let temperature: Bool
    let cough: Bool
    let assessmentOfDoctorServices: Bool
    weak var delegate: DoctorDelegate?
    
    init(name: String, temperature: Bool, cough: Bool, assessmentOfDoctorServices: Bool, delegate: DoctorDelegate?) {
        self.name = name
        self.temperature = temperature
        self.cough = cough
        self.delegate = delegate
        self.assessmentOfDoctorServices = assessmentOfDoctorServices
    }
    
    func goToTheDoctor () {
        delegate?.treatingPatients(patient: self)
    }
    
    func gotSick() {
        if cough == true || temperature == true {
            if temperature == true {
                print("\(name) - мне плохо! У меня температура")
            } else {
                print("\(name) - мне плохо! У меня кашель")
            }
        }
    }
}

class Doctor: DoctorDelegate {
    let antibiotic: String
    let potion: String
    let head: String
    let leg: String
    let belly: String
    
    init(antibiotic: String, potion: String, head: String, leg: String, belly: String) {
        self.antibiotic = antibiotic
        self.potion = potion
        self.head = head
        self.leg = leg
        self.belly = belly
    }
    
    func treatingPatients(patient: Patients) {
        if patient.temperature == true || patient.cough {
            if patient.temperature == true {
                print("""
                    
                    - Что вас беспокоит \(patient.name)?
                    - У меня болит \(leg) и высокая температура
                    Записываем жалобу на боль в ногах и даём \(patient.name) \(antibiotic) от температуры
                    """)
                complaintleg.append(patient)
            } else {
                print("""
                    
                    - Что вас беспокоит \(patient.name)?
                    - У меня болит \(head) и сильный кашель
                    Записываем жалобу на головную боль и даём \(patient.name) \(potion) от кашля
                    """)
                complaintHand.append(patient)
            }
        }
    }
    
    func patientComplaintsReport () {
        print("\nЖалобы пациэнтов Доктора\n------------------------")
        complaintHand.forEach { print("жаловался на головную боль \($0.name)") }
        complaintBelly.forEach { print("жаловался на боль в животе \($0.name)") }
        complaintleg.forEach { print("жаловался на боли в ногах \($0.name)") }
    }
    
    var complaintHand: [Patients] = []
    var complaintleg: [Patients] = []
    var complaintBelly: [Patients] = []
}

class DoctorsFriend: DoctorDelegate {
    let gooseGrass: String
    let valerian: String
    let head: String
    let leg: String
    let belly: String
    
    init(gooseGrass: String, valerian: String, head: String, leg: String, belly: String) {
        self.gooseGrass = gooseGrass
        self.valerian = valerian
        self.head = head
        self.leg = leg
        self.belly = belly
    }
    
    func treatingPatients(patient: Patients) {
        if patient.temperature == true || patient.cough == true {
            if patient.temperature == true {
                print("""
                    
                    - Что вас беспокоит \(patient.name)?
                    - У меня болит \(head) и высокая температура
                    Записываем жалобу на головную боль и даём \(patient.name) \(gooseGrass) от температуры
                    """)
                complaintHand.append(patient)
            } else {
                print("""
                    
                    - Что вас беспокоит \(patient.name)?
                    - У меня болит \(belly) и сильный кашель
                    Записываем жалобу на боль в животе и даём \(patient.name) \(valerian) от кашля
                    """)
                complaintBelly.append(patient)
            }
        }
    }
    
    func patientComplaintsReport () {
        print("\nЖалобы пациэнтов друга Доктора\n-----------------------------")
        complaintHand.forEach { print("жаловалась на головную боль \($0.name)") }
        complaintBelly.forEach { print("жаловалась на боль в животе \($0.name)") }
        complaintleg.forEach { print("жаловалась на боли в ногах \($0.name)") }
    }
    
    var complaintHand: [Patients] = []
    var complaintleg: [Patients] = []
    var complaintBelly: [Patients] = []
}
